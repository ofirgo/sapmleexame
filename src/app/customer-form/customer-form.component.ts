import { Detail } from './../interfaces/detail';
import { Component, OnInit,Input,Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name:string;
  @Input() income:number;
  @Input() years:number;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Detail>();
  @Output() closeEdit = new EventEmitter<null>();
  
  errorMessage:string;
  isError:boolean =false;

  updateParent(){
    let customer:Detail = {id:this.id, name:this.name, income:this.income, years:this.years};
    if(this.years< 0 || this.years>24){
      this.isError=true;
      this.errorMessage = "Please inseret number in rang 0,24";
    }else{
      this.update.emit(customer); 
    }
    if(this.formType == "Add Customer"){
      this.name  = null;
      this.years = null;
      this.income = null; 
    }

  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  constructor() { }

  ngOnInit(): void {
  }

}
