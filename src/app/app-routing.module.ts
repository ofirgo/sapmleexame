import { WelcomeComponent } from './welcome/welcome.component';
import { PostsComponent } from './posts/posts.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { MyPostsComponent } from './my-posts/my-posts.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  {path:'login', component:LoginComponent},
  {path:'signup', component:SignUpComponent},
  {path:'customers', component:CustomersComponent},
  {path:'posts', component:PostsComponent},
  {path:'myposts', component:MyPostsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
