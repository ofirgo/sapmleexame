import { HttpClient } from '@angular/common/http';
import { LiteralExpr } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Comments } from './interfaces/comments';
import { Post } from './interfaces/post';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private urlP ="https://jsonplaceholder.typicode.com/posts/";
  private urlC ="https://jsonplaceholder.typicode.com/comments/";
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  postsCollection:AngularFirestoreCollection;



  public deletePost(userId:string , id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  getPosts(){
   return this.http.get<Post>(this.urlP);
  }
  getComments(){
    return this.http.get<Comments>(this.urlC);
  }

  addPosts(userId:string,id:number,title:string,body:string){
    const post = {id:id, title:title,body:body}; 
    console.log(userId)
    this.userCollection.doc(userId).collection('posts').add(post);
  }
  updatePost(userId:string,id:string,like:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
        like:like
      }
    )
  }

  public getMyPosts(userId){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))

  }

  constructor(private db:AngularFirestore, private http:HttpClient) { }
}
