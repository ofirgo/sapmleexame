import { AuthService } from './../auth.service';
import { PostsService } from './../posts.service';
import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Post } from '../interfaces/post';
import { Router } from '@angular/router';



@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  conUserId:string;
  posts$;
  comments$;
  btntitle:string="Save"
  savestate = [];



savePost(id,title,body){
  this.postsService.addPosts(this.conUserId,id,title,body);
  this.btntitle ="Saved for later viewing"
}

  panelOpenState = false
  constructor(private postsService:PostsService,private authService:AuthService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.conUserId = user.uid;
        console.log(this.conUserId);
        this.posts$ = this.postsService.getPosts()
        this.comments$= this.postsService.getComments()
       
      }

    )

  }

}
