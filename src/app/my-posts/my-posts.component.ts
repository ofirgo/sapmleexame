import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})
export class MyPostsComponent implements OnInit {


  conUserId:string;
  myPosts$;



  public deletePost(id:string){
    this.postsService.deletePost(this.conUserId,id);
  }
  update(id:string,like:number){
    if(isNaN(like)){
      like = 1;
    }else{
      like ++;
    }

    this.postsService.updatePost(this.conUserId,id,like);

  }

  constructor(private postsService:PostsService,private authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.conUserId = user.uid;
        console.log(this.conUserId);
        this.myPosts$ = this.postsService.getMyPosts(this.conUserId);
      }

    )




  }

}
