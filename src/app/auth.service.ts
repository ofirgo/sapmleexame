import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>;


login(email:string, password:string){
  return this.afAuth.signInWithEmailAndPassword(email,password);
}

signUp(email:string, password:string){
  return this.afAuth.createUserWithEmailAndPassword(email,password);
}

logOut(){
  this.afAuth.signOut();
  
}

getUser(): Observable <User | null>{
  return this.user;
}



  constructor(private afAuth:AngularFireAuth) { 
    this.user = this.afAuth.authState;
  }
}
