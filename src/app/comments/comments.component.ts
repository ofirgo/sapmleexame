import { Post } from './../interfaces/post';
import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { PostsService } from '../posts.service';

@Component({
  selector: 'comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  

  @Input() title:string;
  @Input() body:string;
  @Input() id:number;
  @Output() update = new EventEmitter<Post>();
  @Output() closeEdit = new EventEmitter<null>();

  updateParent(){
  let post:Post = {id:this.id, title:this.title, body:this.body};
    this.update.emit(post); 
    console.log(post)
  


}

  constructor() { }

  ngOnInit(): void {

  }
}
