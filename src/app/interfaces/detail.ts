export interface Detail {
    id:string,
    income:number,
    name:string,
    years:number,
    saved?:Boolean,
    result?:string,
}
