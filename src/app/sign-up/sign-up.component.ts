import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  
  email:string;
  password:string;
  errorMesssage:string;
  isError:boolean =false;
  
  
  onSubmit(){
    this.auth.signUp(this.email, this.password).then(
      res =>{
        console.log(res);
        this.router.navigate(['/login']);
      }
    ).catch(
      err => {
        console.log(err);
        this.errorMesssage = err.message;
        this.isError = true;
      } 
    );
  }
  

  constructor(private router:Router,private auth:AuthService) { }

  ngOnInit(): void {
  }

}
