import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  

  private url = "https://r7p9n28val.execute-api.us-east-1.amazonaws.com/beta";
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  customersCollection:AngularFirestoreCollection ;

  public getCustomers(userId){
    this.customersCollection = this.db.collection(`users/${userId}/cutomers`);
    return this.customersCollection.snapshotChanges();
  }

  addCustomer(userId:string,name:string,income:number,years:number){
    const customer = {name:name, income:income,years:years}; 
    this.userCollection.doc(userId).collection('cutomers').add(customer);
  }

  updateCustomer(userId:string,id:string,name:string,years:number,income:number){
    this.db.doc(`users/${userId}/cutomers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        result:null

      }
    )
  }
  updateCustomerResult(userId:string,id:string, result?:string){
    this.db.doc(`users/${userId}/cutomers/${id}`).update(
      {

        result:result

      }
    )
  }

  public deleteCustomer(userId:string , id:string){
    this.db.doc(`users/${userId}/cutomers/${id}`).delete();
  }

  // predict(years:number, income:number):Observable<any>{
  //   let json = {'data':[
  //     {'years':years, 'income':income}
  //   ]}
  //   let body =JSON.stringify(json);
  //   console.log("in predict");
  //   return this.http.post<any>(this.url, body).pipe(
  //     map(res=> { 
  //       console.log(res);
  //       console.log(res.body);
  //       return res.body; 
  //     })
  //   );

  // }

  predict(years:number, income:number):Observable<any>{
    let json = {
      "data": 
        {
          "years": years,
          "income": income
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private db:AngularFirestore, private http:HttpClient) { }
}
