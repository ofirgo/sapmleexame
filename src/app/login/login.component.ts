import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

password:string;
email:string;
errorMesssage:string;
isError:boolean =false;

onSubmit(){
  this.auth.login(this.email, this.password).then(
    res =>{
      console.log('Succesful login;');
      this.router.navigate(['/welcome']);
    }
  ).catch(
    err => {
      console.log(err);
      this.errorMesssage = err.message;
      this.isError = true;
    } 
  );
}


  constructor(private router:Router, private auth:AuthService) { }

  ngOnInit(): void {



  }


}
