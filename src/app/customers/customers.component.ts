import { Detail } from './../interfaces/detail';
import { CustomerService } from './../customer.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {


userId:string;
customers$;
customers:Detail[];
displayedColumns: string[] = ['name', 'years', 'income','Edit','Edit Class','delete','predict','Result'];
addCustomerFormOPen = false;
editstate = [];
rowToEdit:number = -1; 
customerToEdit:Detail = {id:null,name:null, years:null, income:null};
income:number;
years:number;



add(customer:Detail){
  this.customerService.addCustomer(this.userId,customer.name,customer.income,customer.years); 
}

  moveToEditState(index){
      console.log(this.customers[index].name);
      this.customerToEdit.name = this.customers[index].name;
      this.customerToEdit.years = this.customers[index].years;
      this.customerToEdit.income = this.customers[index].income;
      this.rowToEdit = index; 
    }
    updateCustomer(){
          let id = this.customers[this.rowToEdit].id;
          this.customerService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.years,this.customerToEdit.income);
          this.rowToEdit = null;
    }
    update(customer:Detail){
      this.customerService.updateCustomer(this.userId,customer.id,customer.name,customer.years,customer.income);
}

deleteCustomer(id:string){
  this.customerService.deleteCustomer(this.userId,id);
}

updateResult(index){
  this.customers[index].saved = true; 
  let id = this.customers[index].id;
  this.customerService.updateCustomerResult(this.userId,id,this.customers[index].result);
}

predict(index){
  this.customers[index].result = 'Will difault';
  this.years = this.customers[index].years;
  this.income =this.customers[index].income;
  this.customerService.predict( this.years,this.income).subscribe(
    res=>{
        console.log(res);
      if(res > 0.5){
       var result = "Will return";
      }else{
        var result = "Will not return";
      }
      this.customers[index].result = result;
    }
  )
}

  constructor(public authService:AuthService, private customerService:CustomerService) { }

  ngOnInit(): void {
        this.authService.getUser().subscribe(
                user => {
                  this.userId = user.uid;
                  this.customers$ = this.customerService.getCustomers(this.userId);
                  console.log(this.userId)
                  this.customers$.subscribe(
                    docs => {
                     this.customers = [];
                      for(let document of docs){
                        const customer:Detail = document.payload.doc.data();
                        if(customer.result){
                          customer.saved = true; 
                        }
                        customer.id = document.payload.doc.id;
                        this.customers.push(customer);
                      }
                    }
                 )
                }
              )


  }

}
