import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  userId:string;
  email:string;
  password:string;
  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      data =>{
        this.userId = data.uid;
        this.email = data.email;

      }
    )
  }

}
