// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDjsWXHWWQRUu0MYE4Sld0ls_M2aT8zJSM",
    authDomain: "sampleexame.firebaseapp.com",
    projectId: "sampleexame",
    storageBucket: "sampleexame.appspot.com",
    messagingSenderId: "322048804969",
    appId: "1:322048804969:web:fb0648920db66fffeefaf8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
